export interface VerificationRequestPayload {
  senderUid: string;
  recepientEmail: string;
}
