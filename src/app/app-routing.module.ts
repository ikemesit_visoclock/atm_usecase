import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerificationRequestComponent } from './verification-request/verification-request.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'verification-request',
    pathMatch: 'full',
  },
  {
    path: 'verification-request',
    component: VerificationRequestComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
