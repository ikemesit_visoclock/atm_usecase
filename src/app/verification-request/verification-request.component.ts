import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { VerificationRequestService } from './verification-request.service';
import { VerificationRequestPayload } from '../interfaces/vrification-request-payload.interface';

@Component({
  selector: 'app-verification-request',
  templateUrl: './verification-request.component.html',
  styleUrls: ['./verification-request.component.scss'],
})
export class VerificationRequestComponent implements OnInit {
  email!: string;
  responseCode!: string;
  error: string | null = null;
  showQRCode = false;
  hideForm = false;
  showProgressBar = false;
  showSuccess = false;

  baseURL = 'https://us-central1-veriself.cloudfunctions.net/api';
  baseURL2 = 'http://localhost:3000';

  constructor(
    private verificationRequestService: VerificationRequestService,
    private firestore: AngularFirestore
  ) {}

  ngOnInit(): void {
    // this.getDocUpdates();
  }

  async onSubmit() {
    try {
      const querySnapshot = await this.firestore
        .collection('users', (ref) =>
          ref.where('primary_email', '==', `${this.email}`)
        )
        .get()
        .toPromise();

      const doc = querySnapshot.docs[0];

      if (doc === undefined) {
        this.error = 'Email does not exist';
        return;
      }

      const data = doc.data() as any;
      console.log(data);

      const payload: VerificationRequestPayload = {
        recepientEmail: this.email,
        senderUid: 'TlDCPsu69yM0DpMMJ3BgU4R5SKu2',
      };

      this.verificationRequestService.submitEmail(payload).subscribe((res) => {
        console.dir(res);
        if (res.responseCode.length > 0) {
          this.responseCode = res.responseCode;
          this.showProgressBar = true;
          this.showQRCode = true;
          this.hideForm = true;

          this.getDocUpdates(res.uid, res.timestamp);
        }
      });
    } catch (error) {
      console.error(error);
    }
  }

  getDocUpdates(uid: string, timestamp: number) {
    const sseEvents = new EventSource(
      `${this.baseURL2}/verification-request/status?uid=${uid}&timestamp=${timestamp}`
    );

    sseEvents.onmessage = ({ data }) => {
      console.log(data);
      const dataJson = JSON.parse(data);

      if (dataJson?.status === 'VERIFIED') {
        this.showSuccess = true;
        console.log(this.showSuccess);

        sseEvents.close();
      }
    };
  }

  clearError() {
    this.error = null;
  }
}
