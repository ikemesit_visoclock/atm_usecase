import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VerificationRequestPayload } from '../interfaces/vrification-request-payload.interface';

export interface VerificationResponse {
  responseCode: string;
  uid: string;
  timestamp: number;
}

@Injectable({
  providedIn: 'root',
})
export class VerificationRequestService {
  baseURL = 'https://us-central1-veriself.cloudfunctions.net/api';
  baseURL2 = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) {}

  submitEmail(
    payload: VerificationRequestPayload
  ): Observable<VerificationResponse> {
    return this.httpClient.post<VerificationResponse>(
      `${this.baseURL2}/verification-request`,
      payload
    );
  }
}
