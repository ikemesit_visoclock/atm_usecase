export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyA0F6NNaGnxD_DyhhtwNri4aDjDsBHkv2w',
    authDomain: 'veriself.firebaseapp.com',
    databaseURL: 'https://veriself.firebaseio.com',
    projectId: 'veriself',
    storageBucket: 'veriself.appspot.com',
    messagingSenderId: '736750818203',
    appId: '1:736750818203:web:19f0b9d30026d68e31473e',
    measurementId: 'G-LVLGMN2K2H',
  },
};
